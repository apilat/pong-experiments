from config import PORT_HELLO, PORT_GAME
import socket
import random


class PongNetworking():
    def __init__(self, pong):
        self.address = socket.gethostbyname(socket.gethostname())
        self.pong = pong

    def createServer(self):
        self.serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.serverSocket.bind((self.address, PORT_HELLO))
        self.serverSocket.listen(1)
        connection, address = self.serverSocket.accept()
        self.serverSocket.close()
        if connection.recv(1024) != b'hello':
            return
        connection.send(b'hello')
        randomSeed = random.random()
        connection.send(str(randomSeed).encode('ascii'))
        connection.close()
        random.seed(randomSeed)
        self.pong.startGame(0)
        self.connectionAddress = (address[0], PORT_GAME)
        self.createSendingSocket()
        self.createListeningSocket()
        self.listenServer()

    def createClient(self, address):
        self.clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.clientSocket.connect((address, PORT_HELLO))
        self.clientSocket.send(b'hello')
        if self.clientSocket.recv(1024) != b'hello':
            return
        randomSeed = float(self.clientSocket.recv(1024))
        self.clientSocket.close()
        random.seed(randomSeed)
        self.pong.startGame(1)
        self.connectionAddress = (address, PORT_GAME)
        self.createSendingSocket()
        self.createListeningSocket()
        self.listenClient()

    def createListeningSocket(self):
        self.listeningSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.listeningSocket.bind((self.address, PORT_GAME))

    def listenServer(self):
        while True:
            message, address = self.listeningSocket.recvfrom(1024)
            paddlePos = float(message)
            self.pong.movePaddle(paddlePos, 1)

    def listenClient(self):
        while True:
            message, address = self.listeningSocket.recvfrom(1024)
            message = message.decode()
            info = message.split(';')
            self.pong.moveBall([float(x) for x in info[0].split(':')])
            self.pong.movePaddle(float(info[1]), 0)
            self.pong.ballDirection = [float(x) for x in info[2].split(':')]
            self.pong.delay = float(info[3])
            self.pong.running = float(info[4])
            self.pong.score = [int(x) for x in self.pong.score]

    def createSendingSocket(self):
        self.sendingSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    def sendPaddlePosition(self):
        self.sendingSocket.sendto(str(self.pong.paddlePos[1]).encode(),
                                  self.connectionAddress)

    def sendGameInfo(self):
        inf = ''
        inf += ':'.join(str(x) for x in self.pong.ballPos) + ';'
        inf += str(self.pong.paddlePos[0]) + ';'
        inf += ':'.join(str(x) for x in self.pong.ballDirection) + ';'
        inf += str(self.pong.delay) + ';'
        inf += str(self.pong.running) + ';'
        inf += ':'.join(str(x) for x in self.pong.score) + ';'
        self.sendingSocket.sendto(inf.encode(), self.connectionAddress)
