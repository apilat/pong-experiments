from config import HEIGHT, WIDTH
from config import BIG_FONT_SIZE, MEDIUM_FONT_SIZE, SMALL_FONT_SIZE
from config import BALL_RADIUS, BALL_COLOR
from config import PADDLE_PADDING, PADDLE_WIDTH, PADDLE_HEIGHT, PADDLE_COLOR
import tkinter as tk
import tkinter.font as tkfont
import socket


class PongWindow():
    def __init__(self, pong):
        self.pong = pong

    def switchFrame(self, frameNum):
        self.currentFrame = frameNum
        self.windowFrames[frameNum].tkraise()

    def run(self):
        self.windowRoot.mainloop()

    def create(self):
        self.startGui()
        self.createMainMenu()
        self.createNewGameMenu()
        self.createJoinGameMenu()
        self.createGameScreen()
        self.createEndGameScreen()
        self.switchFrame(0)

    def startGui(self):
        self.windowRoot = tk.Tk()
        self.windowRoot.title("Pong")
        self.windowRoot.resizable(0, 0)
        self.windowRoot.geometry(str(WIDTH) + "x" + str(HEIGHT))
        self.windowFrames = []

        self.bigFont = tkfont.Font(size=BIG_FONT_SIZE)
        self.mediumFont = tkfont.Font(size=MEDIUM_FONT_SIZE)
        self.smallFont = tkfont.Font(size=SMALL_FONT_SIZE)

    def createMainMenu(self):
        mainMenuFrame = tk.Frame(self.windowRoot)

        tk.Label(mainMenuFrame, text="Pong Main Menu", font=self.bigFont) \
          .place(x=WIDTH/2, y=HEIGHT*1/3, anchor=tk.CENTER)

        tk.Button(mainMenuFrame, text="Start Local Game", font=self.smallFont,
                  command=self.pong.startLocalGame) \
          .place(x=WIDTH*1/4, y=HEIGHT*4/5, anchor=tk.CENTER)
        tk.Button(mainMenuFrame, text="Create LAN Game", font=self.smallFont,
                  command=self.pong.createLanGame) \
          .place(x=WIDTH*2/4, y=HEIGHT*4/5, anchor=tk.CENTER)
        tk.Button(mainMenuFrame, text="Join LAN Game", font=self.smallFont,
                  command=self.pong.joinLanGame) \
          .place(x=WIDTH*3/4, y=HEIGHT*4/5, anchor=tk.CENTER)

        mainMenuFrame.place(x=0, y=0, width=WIDTH, height=HEIGHT)
        self.windowFrames.append(mainMenuFrame)

    def createNewGameMenu(self):
        newGameFrame = tk.Frame(self.windowRoot)

        localIp = socket.gethostbyname(socket.gethostname())
        tk.Label(newGameFrame, text="Your game address is",
                 font=self.bigFont) \
          .place(x=WIDTH/2, y=HEIGHT*2/7, anchor=tk.CENTER)
        tk.Label(newGameFrame, text=localIp, font=self.mediumFont) \
          .place(x=WIDTH/2, y=HEIGHT*7/20, anchor=tk.CENTER)
        tk.Label(newGameFrame, text="Waiting for connection...",
                 font=self.mediumFont) \
          .place(x=WIDTH/2, y=HEIGHT*3/4, anchor=tk.CENTER)

        newGameFrame.place(x=0, y=0, width=WIDTH, height=HEIGHT)
        self.windowFrames.append(newGameFrame)

    def createJoinGameMenu(self):
        joinGameFrame = tk.Frame(self.windowRoot)

        tk.Label(joinGameFrame, text="Enter game address:", font=self.bigFont)\
          .place(x=WIDTH/2, y=HEIGHT*2/7, anchor=tk.CENTER)
        self.gameAddressValue = tk.StringVar()
        e = tk.Entry(joinGameFrame, textvariable=self.gameAddressValue,
                     font=self.mediumFont)
        e.bind("<Return>",
               lambda _: self.pong.joinLanGame(self.gameAddressValue.get()))
        e.place(x=WIDTH/2, y=HEIGHT*7/20, anchor=tk.CENTER)

        joinGameFrame.place(x=0, y=0, width=WIDTH, height=HEIGHT)
        self.windowFrames.append(joinGameFrame)

    def createGameScreen(self):
        gameScreen = tk.Frame(self.windowRoot)

        self.gameCanvas = tk.Canvas(gameScreen)
        self.gameCanvas.place(x=0, y=0, width=WIDTH, height=HEIGHT)

        self.gameCanvas.create_oval(0, 0, BALL_RADIUS * 2, BALL_RADIUS * 2,
                                    fill=BALL_COLOR, tags='ball')
        self.gameCanvas.create_rectangle(PADDLE_PADDING, 0, PADDLE_PADDING +
                                         PADDLE_WIDTH, PADDLE_HEIGHT,
                                         fill=PADDLE_COLOR, tags='paddle0')
        self.gameCanvas.create_rectangle(WIDTH - (PADDLE_PADDING
                                         + PADDLE_WIDTH), 0, WIDTH -
                                         PADDLE_PADDING, PADDLE_HEIGHT,
                                         fill=PADDLE_COLOR, tags='paddle1')
        self.gameCanvas.create_text(WIDTH / 2, HEIGHT / 15,
                                    font=self.bigFont, tags='score')

        gameScreen.place(x=0, y=0, width=WIDTH, height=HEIGHT)
        self.windowFrames.append(gameScreen)

    def createEndGameScreen(self):
        endGameScreen = tk.Frame(self.windowRoot)

        self.winTextVariable = tk.StringVar()
        tk.Label(endGameScreen, textvariable=self.winTextVariable,
                 font=self.bigFont) \
          .place(x=WIDTH/2, y=HEIGHT*1/3, anchor=tk.CENTER)

        tk.Button(endGameScreen, text="Return to main menu",
                  font=self.mediumFont, command=lambda: self.switchFrame(0))\
          .place(x=WIDTH/2, y=HEIGHT*3/4, anchor=tk.CENTER)

        endGameScreen.place(x=0, y=0, width=WIDTH, height=HEIGHT)
        self.windowFrames.append(endGameScreen)

    def updateEndGameMessage(self, won):
        self.winTextVariable.set("You " + ("won" if won else "lost") + "!")

    def createControls(self):
        self.windowRoot.bind("<Motion>", lambda mouse:
                             self.pong.movePaddle(mouse.y))

    def moveBall(self, ballPos):
        ballCoords = (ballPos[0] - BALL_RADIUS, ballPos[1] - BALL_RADIUS,
                      ballPos[0] + BALL_RADIUS, ballPos[1] + BALL_RADIUS)
        self.gameCanvas.coords('ball', ballCoords)

    def movePaddle(self, paddle, paddlePos):
        if paddle == 0:
            paddleCoords = (PADDLE_PADDING, paddlePos - PADDLE_HEIGHT / 2,
                            PADDLE_PADDING + PADDLE_WIDTH, paddlePos +
                            PADDLE_HEIGHT / 2)
        if paddle == 1:
            paddleCoords = (WIDTH - (PADDLE_PADDING + PADDLE_WIDTH), paddlePos
                            - PADDLE_HEIGHT / 2, WIDTH - PADDLE_PADDING,
                            paddlePos + PADDLE_HEIGHT / 2)
        self.gameCanvas.coords('paddle' + str(paddle), paddleCoords)

    def updateScore(self, score):
        self.gameCanvas.itemconfig('score',
                                   text=str(score[0]) + " : " + str(score[1]))
