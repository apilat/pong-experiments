from config import HEIGHT, WIDTH
from config import BALL_RADIUS, BALL_SPEED, BALL_MAX_BOUNCE_ANGLE
from config import BALL_ACCELERATION
from config import PADDLE_HEIGHT, PADDLE_WIDTH, PADDLE_PADDING
from config import AI_SPEED, TPS
from config import GAME_RESET_DELAY, GAME_START_DELAY, POINTS_TO_WIN
from graphics import PongWindow
from network import PongNetworking
import random
import math
import threading


class Pong():
    def startGui(self):
        self.gui = PongWindow(self)
        self.gui.create()
        self.gui.run()

    def startLocalGame(self):
        self.local = 0
        self.startGame(0)

    def createLanGame(self):
        self.local = 1
        self.gui.switchFrame(1)
        self.startNetworking()
        threading.Thread(target=self.networking.createServer).start()

    def joinLanGame(self, address=None):
        self.local = 2
        self.gui.switchFrame(2)
        self.startNetworking()
        if address is not None:
            threading.Thread(target=self.networking.createClient,
                             args=(address,)).start()

    def startNetworking(self):
        self.networking = PongNetworking(self)

    def startGame(self, player):
        self.gui.switchFrame(3)
        self.score = [0, 0]
        self.gui.updateScore(self.score)
        self.player = player
        self.gui.createControls()
        self.resetGameState()

    def run(self):
        if self.delay > 0:
            self.delay = max(0, self.delay - 1 / TPS)
            threading.Timer(1 / TPS, self.run).start()
        elif self.running:
            self.ballTick()
            if self.local == 0:
                self.playAI()
            if self.local == 1:
                self.networking.sendGameInfo()
            threading.Timer(1 / TPS, self.run).start()

    def playAI(self):
        yDistance = self.paddlePos[1] - self.ballPos[1]
        if abs(yDistance) < AI_SPEED:
            move = -yDistance
        else:
            direction = - abs(yDistance) / yDistance
            move = direction * AI_SPEED
        self.movePaddle(self.paddlePos[1] + move, 1)

    def resetGameState(self):
        self.ballPos = [WIDTH / 2, HEIGHT / 2]
        self.gui.moveBall(self.ballPos)
        self.paddlePos = [HEIGHT / 2, HEIGHT / 2]
        self.gui.movePaddle(0, self.paddlePos[0])
        self.gui.movePaddle(1, self.paddlePos[1])
        ballSeed = random.uniform(-1, 1)
        bounceAngle = ballSeed * BALL_MAX_BOUNCE_ANGLE
        ballDirX = math.cos(bounceAngle) * BALL_SPEED
        ballDirY = math.sin(bounceAngle) * BALL_SPEED
        self.ballDirection = [ballDirX, ballDirY]
        self.ballSpeed = BALL_SPEED
        self.delay = GAME_START_DELAY
        self.running = 1
        self.run()

    def movePaddle(self, pos, paddle=None):
        if paddle is None:
            paddle = self.player
            if self.local == 2:
                self.networking.sendPaddlePosition()
        if pos < PADDLE_HEIGHT / 2:
            pos = PADDLE_HEIGHT / 2
        elif pos > HEIGHT - PADDLE_HEIGHT / 2:
            pos = HEIGHT - PADDLE_HEIGHT / 2
        self.paddlePos[paddle] = pos
        self.gui.movePaddle(paddle, self.paddlePos[paddle])

    def ballTick(self):
        self.ballSpeed += BALL_ACCELERATION
        self.ballPos[0] += self.ballDirection[0]
        self.ballPos[1] += self.ballDirection[1]
        self.checkBallWallCollision()
        self.checkBallPaddleCollision(0)
        self.checkBallPaddleCollision(1)
        self.gui.moveBall(self.ballPos)

    def moveBall(self, pos):
        self.ballPos = pos
        self.gui.moveBall(self.ballPos)

    def finishGame(self, winner):
        self.running = 0
        self.score[winner] += 1
        self.gui.updateScore(self.score)
        if self.score[winner] == POINTS_TO_WIN:
            self.gui.switchFrame(4)
            self.gui.updateEndGameMessage(winner == self.player)
        else:
            threading.Timer(GAME_RESET_DELAY, self.resetGameState).start()

    def checkBallWallCollision(self):
        if self.ballPos[1] - BALL_RADIUS <= 0:
            self.ballDirection[1] = abs(self.ballDirection[1])
        if self.ballPos[1] + BALL_RADIUS >= HEIGHT:
            self.ballDirection[1] = -abs(self.ballDirection[1])
        if self.ballPos[0] < 0 or self.ballPos[0] > WIDTH:
            self.finishGame(1 if self.ballPos[0] < 0 else 0)

    def checkBallPaddleCollision(self, paddle):
        paddleX = PADDLE_PADDING + PADDLE_WIDTH / 2 if paddle == 0 else \
                  WIDTH - (PADDLE_PADDING + PADDLE_WIDTH / 2)
        paddleY = self.paddlePos[paddle]
        ballX = self.ballPos[0]
        ballY = self.ballPos[1]
        distanceX = abs(ballX - paddleX)
        distanceY = abs(ballY - paddleY)
        if distanceX > PADDLE_WIDTH / 2 + BALL_RADIUS or \
           distanceY > PADDLE_HEIGHT / 2 + BALL_RADIUS:
            return
        cornerDistanceSq = (distanceX - PADDLE_WIDTH / 2) ** 2 + \
                           (distanceY - PADDLE_HEIGHT / 2) ** 2
        if distanceX <= PADDLE_WIDTH / 2 or distanceY <= PADDLE_HEIGHT / 2 or \
           cornerDistanceSq <= BALL_RADIUS ** 2:
            relativeYDistance = paddleY - ballY
            normalizedYDistance = relativeYDistance / PADDLE_HEIGHT
            bounceAngle = normalizedYDistance * BALL_MAX_BOUNCE_ANGLE
            ballDirX = math.cos(bounceAngle) * self.ballSpeed \
                * (1 if paddle == 0 else -1)
            ballDirY = -math.sin(bounceAngle) * self.ballSpeed
            self.ballDirection = [ballDirX, ballDirY]


if __name__ == "__main__":
    game = Pong()
    game.startGui()
